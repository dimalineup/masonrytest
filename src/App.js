import React from 'react';

// styles
import useStyles from './styles';
import { getDataStart } from './actions';
import List from './components/ItemList';

function App() {
  const [data, dataSet] = React.useState([]);
  React.useEffect(() => {
    getDataStart(dataSet);
  }, []);
  let classes = useStyles();
  console.log(data);
  return (
    <div className="App">
      <div className={classes.content}>
        <div className={classes.title}>
          <div className={classes.titleHeadText}>
            Candy cotton candy sesame snaps biscuit
          </div>
          <p className={classes.titleText}>
            Candy cotton candy sesame{' '}
            <span className={classes.titleTextGrey}>#Design</span>
            <span className={classes.titleTextGrey}>#HR</span> biscuit dessert
            topping halvah marshmallow gummies. Pie toffee dragée chocolate
            toffee biscuit. Icing chocolate cake ice cream jelly beans chocolate
            cake soufflé candy.
          </p>
        </div>

        <List data={data} />
      </div>
    </div>
  );
}

export default App;
