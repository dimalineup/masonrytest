export const getDataStart = async dataSet => {
  const res = await fetch(`https://5c07ecd0646dca0013f87e8b.mockapi.io/flow`, {
    method: 'GET',
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: new Headers(),
    redirect: 'follow',
    referrer: 'no-referrer'
  });
  const data = await res.json();

  dataSet(data);
};
