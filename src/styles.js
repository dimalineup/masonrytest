import { makeStyles } from '@material-ui/styles';

export default makeStyles(theme => ({
  content: {
    flexGrow: 1,
    maxWidth: 1040,
    margin: '0 auto',
    overflow: 'auto'
  },
  title: {},
  titleHeadText: {
    fontFamily: 'Inter',
    fontWeight: 'bold',
    fontSize: 24,
    letterSpacing: 0.3,
    color: '#000000'
  },

  titleText: {
    fontFamily: 'Inter',
    fontWeight: 'normal',
    fontSize: 14,
    maxWidth: 600,
    letterSpacing: 0.3,
    color: '#656974'
  },
  itemsContent: {
    columnCount: 4,
    columnGap: '1em',
    display: 'grid',
    gridAutoFlow: 'column'
  },
  imgIcon: {
    width: 12,
    height: 9.5,
    marginRight: 4
  },
  itemContent: {
    overflow: 'hidden',
    borderRadius: 5,
    marginBottom: 16,
    height: 'auto',
    display: 'block',
    width: 'calc(25% - 20px)',
    background: '#F5F6F7',
    '@media (max-width: 980px)': {
      width: 'calc(33.3% - 20px)'
    },
    '@media (max-width: 780px)': {
      width: 'calc(50% - 20px)'
    },
    '@media (max-width: 480px)': {
      width: 'calc(100% - 20px)'
    }
  },
  itemImg: {
    width: '100%',
    height: ({ heightImage = 'auto' }) => heightImage,
    display: 'block',
    borderRadius: 5,
    marginBottom: ({ marginBotImg = 8 }) => marginBotImg
  },
  itemImgW: {
    width: '100%',
    height: 'auto',
    display: 'block',
    borderRadius: 5,
    marginBottom: 8
  },
  itemInfo: {
    padding: '0 16px 11px',
    '@media (max-width: 780px)': {
      padding: '0 10px 11px'
    }
  },
  itemInfoHeadText: {
    fontSize: '15px',
    marginBottom: 8,
    color: '#000000'
  },
  itemPerson: {
    display: 'flex',

    justifyContent: 'space-between'
  },
  itemPersonLeft: {
    display: 'flex'
  },
  itemPersonAvatar: {
    maxWidth: 18,
    maxHeight: 18,
    borderRadius: '50%',
    marginRight: '8px'
  },
  itemPersName: {
    color: '#656974',
    fontSize: 14,
    maxWidth: 115,
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    '@media (max-width: 780px)': {
      fontSize: 13,
      maxWidth: 100
    }
  },
  itemPersonRight: {
    display: 'flex'
  },
  count: {
    fontSize: 12,
    color: '#656974'
  },
  itemPersonLikes: {
    marginRight: 8
  },
  itemWrap: {
    display: 'block',
    height: 'auto',
    width: '100%',
    marginBottom: 6
  },
  titleTextGrey: {
    fontSize: 13,
    margin: '0 10px',
    padding: '3px 10px',
    borderRadius: 6,
    color: '#656974',
    background: '#F5F6F7'
  }
}));
