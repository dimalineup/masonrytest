import React from 'react';
import propTypes from 'prop-types';
import imgWait from '../img/WAIT-Sign.jpg';
import useStyles from '../styles';
function RenderImage({ imgSrc }) {
  const [img, dataSet] = React.useState('wait');

 
  let params = {};
  if (img === 'wait') {
    params = { heightImage: 0, marginBotImg: 0 };
  }
  const classes = useStyles(params);
  console.log('render');
  return (
    <div className={classes.itemWrap}>
      {img !== false && (
        <img
          className={classes.itemImg}
          src={imgSrc}
          alt="img"
          onError={() => {
            console.log('false');
            dataSet(false);
          }}
          onLoad={() => {
            console.log('true');
            dataSet(true);
          }}
        />
      )}
      {img === 'wait' && (
        <img className={classes.itemImgW} src={imgWait} alt="dogIcon" />
      )}
    </div>
  );
}

RenderImage.propTypes = {
  imgSrc: propTypes.string.isRequired
};
export default RenderImage;
