import React from 'react';
import ShapeIcon from '../img/Shape.png';

import CommentIcon from '../img/Rectangle.png';
import RenderImage from './RenderImage';

// styles
import useStyles from '../styles';

function Item(item) {
  const classes = useStyles({
    width: 300,
    display: 'block',
    margin: 20
  });

  return (
    <div className={classes.itemContent}>
      <div className={classes.itemWrap}>
        <RenderImage imgSrc={item.imeItem} />
      </div>

      <div className={classes.itemInfo}>
        <div className={classes.itemInfoHeadText}>{item.text}</div>
        <div className={classes.itemPerson}>
          <div className={classes.itemPersonLeft}>
            <div>
              <img
                className={classes.itemPersonAvatar}
                src={item.ownerImage}
                alt="some"
              />
            </div>
            <div className={classes.itemPersName}>{item.ownerName}</div>
          </div>
          <div className={classes.itemPersonRight}>
            <div className={classes.itemPersonLikes}>
              <img className={classes.imgIcon} src={ShapeIcon} alt="some" />
              <span className={classes.count}>{item.likes}</span>
            </div>
            <div className={classes.itemPersComments}>
              <img className={classes.imgIcon} src={CommentIcon} alt="some" />
              <span className={classes.count}>{item.comments}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Item;
