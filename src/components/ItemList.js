import React from 'react';

import Item from './Item';
import Masonry from 'react-masonry-component';
function List({ data }) {
  const masonryOptions = {
    transitionDuration: 0,
    gutter: 16
  };

  const imagesLoadedOptions = { background: '.my-bg-image-el' };
  return (
    <Masonry
      className={'my-gallery-class'}
      elementType={'div'}
      options={masonryOptions}
      enableResizableChildren={true}
      updateOnEachImageLoad={true}
      disableImagesLoaded={false}
      imagesLoadedOptions={imagesLoadedOptions}
    >
      {data.map((item, index) => (
        <Item {...{ ...item, imeItem: item.image }} key={`in${index}`} />
      ))}
    </Masonry>
  );
}

export default List;
